﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epolis.Models
{
   
    public class MperluasanVM
    {
        public int ID { get; set; }
        public string KODEOKUPASI { get; set; }
      
        public string NAMAOKUPASI { get; set; }
        public string KODEPERLUASAN { get; set; }
        public string DESKRIPSI { get; set; }       
        public decimal RATEPERLUASAN { get; set; }   
       
       
    }
}
