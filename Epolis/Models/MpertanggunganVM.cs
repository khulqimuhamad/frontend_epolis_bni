﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epolis.Models
{
    
    public class MpertanggunganVM
    {
        public int ID { get; set; }
        [Display(Name = "Kode Okupasi")]
        public string KODEOKUPASI { get; set; }
        [Display(Name = "Nama Okupasi")]
        public string NAMAOKUPASI { get; set; }
        [Display(Name = "Kode Pertanggungan")]
        public string KODEPERTANGGUNGAN { get; set; }
        [Display(Name = "Deskripsi")]
        public string DESKRIPSI { get; set; }       
                  
        public bool? ISDELETED { get; set; }
       
    }
}
