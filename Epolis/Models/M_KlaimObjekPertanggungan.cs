﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_KlaimObjekPertanggungan
    {
        public int Id { get; set; }
        public int? Id_ObjekPenutupan { get; set; }
        public bool? StatusObjekKlaim { get; set; }
        //public TimeSpan? WaktuKejadian { get; set; }
        public DateTime? TanggalKejadian { get; set; }
        public decimal? TaksiranKerugian { get; set; }
        public string PenyebabKejadian { get; set; }
        public string KronologisKejadian { get; set; }
        public string CatatanDariBank { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public bool? IsActive { get; set; }
    }
}
