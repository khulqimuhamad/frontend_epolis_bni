﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_FilePendukung
    {
        public Int64 Number { get; set; }
        public Int64 Id { get; set; }
        public Int64? IdObjekKlaim { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string DownloadPath { get; set; }
        public float FileSize { get; set; }
        public string FileType { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdateTime { get; set; }
        public int? UpdatedById { get; set; }
        public int? CreatedById { get; set; }
        public bool? IsActive { get; set; }
    }
}
