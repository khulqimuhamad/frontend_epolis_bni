﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MTagihanPremi
    {
        public int id { get; set; }
        public Int64 Number { get; set; }
        public string NamaAsuradur { get; set; }
        public string NamaBroker { get; set; }
        public string NoPolis { get; set; }
        public string NamaDebitur { get; set; }
        public DateTime? TanggalTerbitPolis { get; set; }
        public DateTime? TanggalJatuhTempo { get; set; }
        public long? TotalPertanggungan { get; set; }
        public string Valuta { get; set; }
        public double? NilaiPremi { get; set; }
        public double? TarifPremi { get; set; }
        public string MetodePembayaran { get; set; }
        public string NoGiroAsuradur { get; set; }
        public string BnikantorCabang { get; set; }
        public bool? IsActive { get; set; }
    }
}
