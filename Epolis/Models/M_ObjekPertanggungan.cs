﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_ObjekPertanggungan
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int? Tpenutupanid { get; set; }
        public int? Tkontrakasuransiid { get; set; }
        public string Namatertanggung { get; set; }
        public string Lokasiobjektertanggung { get; set; }
        public string Matauang { get; set; }
        public decimal Jumlahpertanggungan { get; set; }
        public DateTime Tanggalmulai { get; set; }
        public DateTime Tanggalakhir { get; set; }
        public int? Updatedbyid { get; set; }
        public DateTime? Updatedtime { get; set; }
        public int? Createdbyid { get; set; }
        public DateTime? Createdtime { get; set; }
        public bool? Isdelete { get; set; }
        public int? Deletedbyid { get; set; }
        public DateTime? Deletedtime { get; set; }
        public decimal? Taksasi { get; set; }
        public int? Kelas { get; set; }
        public string Namaokupasi { get; set; }
        public int? Spenutupanid { get; set; }
        public string Catatan { get; set; }
        public bool? Tbrokerid { get; set; }
        public bool? Isditutupasuransi { get; set; }
        public int? Kelaskonstruksiid { get; set; }
        public bool? StatusObjekKlaim { get; set; }
        public DateTime? TanggalKejadian { get; set; }
        public decimal? TaksiranKerugian { get; set; }
        public string PenyebabKejadian { get; set; }
        public string KronologisKejadian { get; set; }
        public string CatatanDariBank { get; set; }

    }
}
