﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MuraianObyekPertanggungan
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int IdKlaim { get; set; }
        public int TPENUTUPANID { get; set; }
        public int TKONTRAKASURANSIID { get; set; }
        public string NAMATERTANGGUNG { get; set; }
        public string LOKASIOBJEKTERTANGGUNG { get; set; }
        public string MATAUANG { get; set; }
        public decimal JUMLAHPERTANGGUNGAN { get; set; }
        public string JUMLAHPERTANGGUNGANText { get; set; }
        public DateTime? TANGGALMULAI { get; set; }
        public DateTime? TANGGALAKHIR { get; set; }
        public int? Id_ObjekPenutupan { get; set; }
        public bool? StatusObjekKlaim { get; set; }
        public string StatusObjekKlaimText { get; set; }
        //public TimeSpan? WaktuKejadian { get; set; }
        public DateTime? TanggalKejadian { get; set; }
        public string PenyebabKejadian { get; set; }
        public string KronologisKejadian { get; set; }
        public string CatatanDariBank { get; set; }
        public decimal? TaksiranKerugian { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public bool? IsActive { get; set; }
        public string Komentar { get; set; }
    }
}
