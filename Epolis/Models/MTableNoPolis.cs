﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MTableNoPolis
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public string NamaAsuradur { get; set; }
        public string NamaBroker { get; set; }
        public string NoPolis { get; set; }
        public string NamaDebitur { get; set; }
    }
}
