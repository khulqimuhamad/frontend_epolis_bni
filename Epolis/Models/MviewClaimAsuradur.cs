﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MviewClaimAsuradur
    {
        public int? Id_Klaim { get; set; }
        public int? Id_Penutupan { get; set; }
        public long TotalNilaiPertanggungan { get; set; }
        public string TotalNilaiPertanggunganText { get; set; }
        public decimal JumlahPertanggungan { get; set; }
        public string JumlahPertanggunganText { get; set; }
        public decimal NilaiKlaim { get; set; }
        public string NilaiKlaimText { get; set; }
        public DateTime? TanggalPembayaran { get; set; }
        public string NomorRekeningTujuan { get; set; }
        public string NamaRekening { get; set; }
        public string Penjelasan { get; set; }
        public bool? statusKeputusan { get; set; }
        public string penjelasanBanding { get; set; }
        public int Id_ApproverUB { get; set; }
        public int Id_ReviewerUB { get; set; }
        public int Id_InputerADK { get; set; }
        public string komentar { get; set; }
    }

    public class MvalidasiDiterima
    {
        public int Id { get; set; }
        public bool? PembayaranKlaim { get; set; }
        public DateTime? TanggalDiterima { get; set; }
    }
}
