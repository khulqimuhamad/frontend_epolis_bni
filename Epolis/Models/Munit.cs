﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epolis.Models
{
    [Table("MUNIT")]
    public partial class Munit
    {
        public int ID { get; set; }
        [Display(Name = "Kode Unit")]
        public string UNITCODE { get; set; }
        [Display(Name = "Nama Unit ")]
        public string UNITNAME { get; set; }
        [Display(Name = "Alamat ")]
        public string ADDRESS { get; set; }
        [Display(Name = "Email ")]
        public string EMAIL { get; set; }
        [Display(Name = "Telepon ")]
        public string TELEPON { get; set; }
        public bool? ISACTIVE { get; set; }
        public int? UPDATEDBYID { get; set; }
        public DateTime? UPDATEDTIME { get; set; }
        public bool? ISDELETED { get; set; }
        public int? DELETEDBYID { get; set; }
        public DateTime? DELETEDTIME { get; set; }
        public int? CREATEDBYID { get; set; }
        public DateTime? CREATEDTIME { get; set; }

    }
}
