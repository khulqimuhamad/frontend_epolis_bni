﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MTablePenyelesaianPremi
    {
        public int id { get; set; }
        public Int64 Number { get; set; }
        public string NoPolis { get; set; }
        public string JenisPembayaran { get; set; }
        public long? Tagihan { get; set; }
        public long? PembayaranNominal { get; set; }
        public DateTime? TanggalBayar { get; set; }
        public int? BayarKe { get; set; }
        public string NoRekBayar { get; set; }
        public string AtasNamaRek { get; set; }
        public string Keterangan { get; set; }
        public int? JumlahAngsuran { get; set; }
        public DateTime? Createdtime { get; set; }
        public int? CreatedById { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? UpdatedById { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }

    }
}
