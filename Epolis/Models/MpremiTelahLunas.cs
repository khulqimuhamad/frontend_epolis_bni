﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MpremiTelahLunas
    {
        public Int64 Number { get; set; }
        public DateTime? TglPolis { get; set; }
        public DateTime? Tanggal { get; set; }
        public DateTime? TanggalBayar { get; set; }
        public string NamaAsuransi { get; set; }
        public string NoPolisLama { get; set; }
        public string NoPolisBaru { get; set; }
        public string NamaDebitur { get; set; }
        public decimal TagihanPremi { get; set; }
        public string TagihanPremiText { get; set; }
        public string JenisPembayaran { get; set; }
        public string Status { get; set; }
    }
}
