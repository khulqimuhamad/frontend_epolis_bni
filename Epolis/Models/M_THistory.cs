﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_THistory
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int? id_Klaim { get; set; }
        public int? id_Pengirim { get; set; }
        public int? idKewenangan { get; set; }
        public int? id_Inputer { get; set; }
        public int? id_Reviewer { get; set; }
        public int? idA_pprover { get; set; }
        public int? id_Unit { get; set; }
        public string komentar { get; set; }
        public int? id_Status { get; set; }
        public DateTime? createdTime { get; set; }
        public DateTime? updatedTime { get; set; }
        public int? id_Penerima { get; set; }
        public string npp { get; set; }
        public string nama { get; set; }
        public string role { get; set; }
        public string status { get; set; }
        public string tanggal { get; set; }
    }
}
