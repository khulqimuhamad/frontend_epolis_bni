﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epolis.Models
{
   
    public class MokupasiVM
    {
        public int ID { get; set; }
        public string VALUE { get; set; }
        [Display(Name = "Kode Okupasi")]
        public string KODEOKUPASI { get; set; }
        [Display(Name = "Nama Okupasi")]
        public string NAMAOKUPASI { get; set; }
        [Display(Name = "Std Kelas 1")]
        public decimal KELAS1 { get; set; }
        [Display(Name = "Std Kelas 2")]
        public decimal KELAS2 { get; set; }
        [Display(Name = "Std Kelas 3")]
        public decimal KELAS3 { get; set; }
       
    }
}
