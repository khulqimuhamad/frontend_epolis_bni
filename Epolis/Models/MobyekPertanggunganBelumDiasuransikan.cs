﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MobyekPertanggunganBelumDiasuransikan
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int TPENUTUPANID { get; set; }
        public DateTime? TanggalPK { get; set; }
        public string NamaDebitur { get; set; }
        public string NoPK { get; set; }
        public string CIF { get; set; }
        public string NamaRM { get; set; }
        public decimal TotalPertanggungan { get; set; }
        public string TotalPertanggunganText { get; set; }
        public decimal NilaiPertanggunganDiskresi { get; set; }
        public decimal NilaiPertanggunganDiskresiText { get; set; }
        public string Alasan { get; set; }
    }
}
