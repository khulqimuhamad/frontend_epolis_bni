﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.ViewModels
{
    public class LoadVM
    {
        public string Column { get; set; }
        public string Filter { get; set; }
        public string Orderby { get; set; }
        public int Firstrow { get; set; }
        public int Secondrow { get; set; }
        public int Param { get; set; }
        public string Params { get; set; }
        public string tokken { get; set; }
        public int? userId { get; set; }
        public int? role_Id { get; set; }
        public string TypeSearch { get; set; }
        public string KodeSearch { get; set; }
        public string NameSearch { get; set; }
        public string SortCoulumn { get; set; }
        public string SortCoulumnDir { get; set; }
        public int PageNumber { get; set; }
        public int RowPage { get; set; }
        public int isEndorsement { get; set; }
        public int Id_PK { get; set; }
        public int Id_Polis { get; set; }
        public int Id_Penutupan { get; set; }
        public string id { get; set; }
        //public int Id { get; set; }
        public string Ids { get; set; }
        public string cif { get; set; }
        public int status { get; set; }
        public string NamaDebitur { get; set; }
        public string NamaRM { get; set; }
    }

    public class LoadVMint
    {
        public int id { get; set; }
        public string Search { get; set; }
        public string SortCoulumn { get; set; }
        public string SortCoulumnDir { get; set; }
        public int PageNumber { get; set; }
        public int RowPage { get; set; }
    }
}
