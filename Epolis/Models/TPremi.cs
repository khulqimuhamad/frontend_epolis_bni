﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class TPremi
    {
        public int ID { get; set; }
        public string NamaAsuradur { get; set; }
        [Display(Name = "Nama Asuradur")]
        [Required(ErrorMessage = "Nama lookup harus diisi")]
        public string NamaBroker { get; set; }
        [Display(Name = "Nama Broker")]
        [Required(ErrorMessage = "Nilai lookup harus diisi")]
        public string NoPolis { get; set; }
        [Display(Name = "No Polis")]
        [Required(ErrorMessage = "Urutan lookup harus diisi")]
        public string NamaDebitur { get; set; }
        [Display(Name = "Nama Debitur")]
        [Required(ErrorMessage = "Urutan lookup harus diisi")]
        public string TanggalTerbitPolis { get; set; }
        [Display(Name = "Tanggal Terbit Polis")]
        [Required(ErrorMessage = "Urutan lookup harus diisi")]
        public string TanggalJatuhTempoPolis { get; set; }
        [Display(Name = "Tanggal Jatuh Tempo Polis")]
        [Required(ErrorMessage = "Urutan lookup harus diisi")]
        public string TotalPertanggungan { get; set; }
        [Display(Name = "TSI / Total Pertanggungan")]
        [Required(ErrorMessage = "Urutan lookup harus diisi")]
        public int? ORDERBY { get; set; }
        public int? UPDATEDBYID { get; set; }
        public DateTime? UPDATEDTIME { get; set; }
        public bool? ISDELETED { get; set; }
        public int? DELETEDBYID { get; set; }
        public DateTime? DELETEDTIME { get; set; }
        public DateTime? CREATEDTIME { get; set; }
        public int? CREATEDBYID { get; set; }

        public bool? ISACTIVE { get; set; }
    }
}
