﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_TPenutupan
    {
        public int Id { get; set; }
        public int? IdPenutupan { get; set; }
        public string LokasiObjekPertanggungan { get; set; }
        public long? TotalNilaiPertanggungan { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? CreatedById { get; set; }
        public int? UpdatedById { get; set; }
        public bool? IsActive { get; set; }
        public int? IdReviewer { get; set; }
        public int? IdApprover { get; set; }
        public int? IdInputerAdk { get; set; }
        public int? IdStatus { get; set; }
        public decimal? TaksiranKerugian { get; set; }
        public int? IdReviewerAdk { get; set; }
        public int? IdApproverAdk { get; set; }
    }
}
