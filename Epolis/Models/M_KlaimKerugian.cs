﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class M_KlaimKerugian
    {


        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int Asuradurid { get; set; }
        public int Brokerid { get; set; }
        public int TnasabahID { get; set; }
        public string Namadebitur { get; set; }
        public string Namabroker { get; set; }
        public string Alamatbroker { get; set; }
        public string Namaperusahaan { get; set; }
        public string Alamatperusahaan { get; set; }
        public string Jenispenutupan { get; set; }
        public string Noregpenutupan { get; set; }
        public string Orderdarikota { get; set; }
        public string Adminid { get; set; }
        public DateTime? Tglinput { get; set; }
        public DateTime? Tglotorisasi { get; set; }
        public DateTime? Tanggalmulai { get; set; }
        public DateTime? Tanggalakhir { get; set; }
        public int Status { get; set; }
        public string Noskk { get; set; }
        public DateTime? Tglskk { get; set; }
        public DateTime? Tglpk { get; set; }
        public DateTime? Tglpolis { get; set; }
        public string Nopk { get; set; }
        public string Nopolis { get; set; }
        public string Cif { get; set; }
        public string Komentar { get; set; }
        public bool? Isbroker { get; set; }
        public bool? Bankersclause { get; set; }
        public int? Isupdatepenutupanrenewal { get; set; }
        public int? Updatedbyid { get; set; }
        public DateTime? Updatedtime { get; set; }
        public bool? IsActive { get; set; }
        public bool? Isdeleted { get; set; }
        public int? Deletedbyid { get; set; }
        public DateTime? Deletedtime { get; set; }
        public DateTime? Createdtime { get; set; }
        public int? Createdbyid { get; set; }
        public decimal? Totalpertanggungan { get; set; }
        public string Pembayaran { get; set; }
        public string Namaunit { get; set; }
        public int? Jumlahangsuran { get; set; }
        public bool? Statuspremi { get; set; }
        public bool? Statusklaim { get; set; }

        public int? Id_Penutupan { get; set; }
        public string LokasiObjekPertanggungan { get; set; }
        public long? TotalNilaiPertanggungan { get; set; }
        public int? Id_Reviewer { get; set; }
        public string Reviewer { get; set; }
        public int? Id_Approver { get; set; }
        public string Approver { get; set; }
        public int? Id_InputerADK { get; set; }
        public string InputerADK { get; set; }
        public int? Id_Status { get; set; }
        public decimal? TaksiranKerugian { get; set; }
        public int? Id_ReviewerADK { get; set; }
        public string ReviewerADK { get; set; }
        public int? Id_ApproverADK { get; set; }
        public string ApproverADK { get; set; }
        public int? IdRMPengelola { get; set; }
        public string RMPengelola { get; set; }
    }
}
