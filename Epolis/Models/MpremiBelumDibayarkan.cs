﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MpremiBelumDibayarkan
    {
        public Int64 Number { get; set; }
        public int Id { get; set; }
        public int Id_Penutupan { get; set; }
        public decimal PremiBelumDibayar { get; set; }
        public string PremiBelumDibayarText { get; set; }
        public decimal TotalPertanggungan { get; set; }
        public string TotalPertanggunganText { get; set; }
        public string Alasan { get; set; }
        public DateTime? TanggalJatuhTempo { get; set; }
        public string NamaAsuransi { get; set; }
        public string NoPolisLama { get; set; }
        public string NoPolisBaru { get; set; }
        public string NamaNasabah { get; set; }
        public string RMPengelola { get; set; }
    }
}
