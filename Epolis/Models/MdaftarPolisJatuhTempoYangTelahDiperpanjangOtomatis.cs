﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MdaftarPolisJatuhTempoYangTelahDiperpanjangOtomatis
    {
        public Int64 Number { get; set; }
        public int Id_Penutupan { get; set; }
        public DateTime? PeriodeJatuhTempo { get; set; }
        public string NoPolisLama { get; set; }
        public string NoPolisBaru { get; set; }
        public string NamaTertanggung { get; set; }
        public decimal TSI { get; set; }
        public string TSIText { get; set; }
        public decimal Premi { get; set; }
        public string PremiText { get; set; }
        public string NamaAsuransi { get; set; }
        public string RMPengelola { get; set; }
        public string Status { get; set; }
    }
}
