﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MresponKlaim
    {
        public Int64 Number { get; set; }
        public int? Id_Klaim { get; set; }
        public int? Id_Penutupan { get; set; }
        public string NoOrder { get; set; }
        public string NoSKK { get; set; }
        public string NoPolis { get; set; }
        public string NoPolisBaru { get; set; }
        public string CIF { get; set; }
        public string NamaDebitur { get; set; }
        public string RMPenutupan { get; set; }
        public string RMKlaim { get; set; }
        public string StatusPembayaranPremi { get; set; }
        public string StatusKlaim { get; set; }
    }
}
