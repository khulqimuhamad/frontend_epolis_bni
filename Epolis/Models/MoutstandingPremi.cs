﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Models
{
    public class MoutstandingPremi
    {
        public Int64 Number { get; set; }
        public int Id_Penutupan { get; set; }
        public DateTime? TglPolis { get; set; }
        public DateTime? TanggalSudahBayar { get; set; }
        public string NamaAsuransi { get; set; }
        public string NoPolisLama { get; set; }
        public string NoPolisBaru { get; set; }
        public string NamaNasabah { get; set; }
        public decimal TagihanPremi { get; set; }
        public string TagihanPremiText { get; set; }
        public decimal SudahDibayarkan { get; set; }
        public string SudahDibayarkanText { get; set; }
        public decimal BelumDibayarkan { get; set; }
        public string BelumDibayarkanText { get; set; }
        public string RMPengelola { get; set; }
        public string Status { get; set; }
    }
}
