﻿using System;
using System.Linq;

namespace epolis.Helper
{
    public class SessionConst
    {
        public const string ReturnUrl = "ReturnUrl";

        public const string jwt_Token = "jwt_Token";
        public const string jwt_Refresh = "jwt_Refresh";
        public const string Npp = "Npp";

    }
}