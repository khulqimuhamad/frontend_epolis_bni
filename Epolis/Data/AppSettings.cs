﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Data
{
    public class AppSettings
    {
        public class Url
        {
            public string ApiUrl { get; set; }
        }
    }
}
