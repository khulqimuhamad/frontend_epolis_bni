﻿using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Epolis.Component
{
    public class ParentController : Controller
    {
        private readonly EpolisContext _context;
        private readonly IConfiguration _configuration;

        public ParentController(IConfiguration config, EpolisContext context)
        {
            _context = context;
            _configuration = config;
        }

        public ParentController() { }

        #region Select Data By ID For Dropdown Approver
        public IEnumerable<DataDropdownServerSide> SelectDataApprover(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:approver"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown Reviewer
        public IEnumerable<DataDropdownServerSide> SelectDataReviewer(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:reviewer"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown InputerADK
        public IEnumerable<DataDropdownServerSide> SelectDataInputerADK(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:inputerADK"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown ReviewerADK
        public IEnumerable<DataDropdownServerSide> SelectDataReviewerADK(string Id, EpolisContext _context)
        {
            req_dataDropdownById_ViewModel model = new req_dataDropdownById_ViewModel()
            {
                id = int.Parse(Id)
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:GetDropdownReviewerADK"];
            //var url = "http://localhost:5500/api/Tklaim/GetByIdDropdownReviewerADK";
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown ApproverADK
        public IEnumerable<DataDropdownServerSide> SelectDataApproverADK(string Id, EpolisContext _context)
        {
            req_dataDropdownById_ViewModel model = new req_dataDropdownById_ViewModel()
            {
                id = int.Parse(Id)
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:GetDropdownApproverADK"];
            //var url = "http://localhost:5500/api/Tklaim/GetByIdDropdownApproverADK";
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown RMPengelola
        public IEnumerable<DataDropdownServerSide> SelectDataRMPengelola(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };

            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:rmPengelola"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion

        #region Select Data By ID For Dropdown KelasKonstruksi
        public IEnumerable<DataDropdownServerSide> SelectDataKelasKonstruksi(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:kelasKonstruksi"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion
        #region Select Data By ID For Dropdown Okupasi
        public IEnumerable<DataDropdownServerSide> SelectDataOkupasi(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:okupasi"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;
        }
        #endregion
        #region Select Data By ID For Dropdown Perluasan
        public IEnumerable<DataDropdownServerSide> SelectDataPerluasan(string Id, EpolisContext _context)
        {
            req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
            {
                id = Id,
                rowpage = 10,
                page = "1"
            };
            var token = HttpContext.Session.GetString("TOKEN");
            var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:perluasan"];
            (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
            ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
            data = data != null ? data : new ListDataDropdownServerSide();
            return data.items;

            //ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

            //List<int> selected = new List<int>();

            //foreach (var item in data.items) {
            //    selected.Add(item.id);
            //};

            //return selected;
        }
        #endregion
    }
}
