#pragma checksum "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Premi\_CreatePenyelesaianPremi.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0bf6483c3c8fb259d34c15e6f7a0cee0ba14e859"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Premi__CreatePenyelesaianPremi), @"mvc.1.0.view", @"/Views/Premi/_CreatePenyelesaianPremi.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0bf6483c3c8fb259d34c15e6f7a0cee0ba14e859", @"/Views/Premi/_CreatePenyelesaianPremi.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"91e3649ee4e062e36c4124dd594d5e54108a11d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Premi__CreatePenyelesaianPremi : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Epolis.Models.MTableNoPolis>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-dark pull-left"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("<div class=\"row\" style=\"padding-top:20px;\">\n");
#nullable restore
#line 3 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Premi\_CreatePenyelesaianPremi.cshtml"
       Html.RenderPartial("_TableNoPolis"); 

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\n<div class=\"row\" style=\"padding-top:20px;\">\n");
#nullable restore
#line 6 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Premi\_CreatePenyelesaianPremi.cshtml"
       Html.RenderPartial("_TermPembayaranPremi"); 

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\n<div class=\"row\" style=\"padding-top:20px;\">\n");
#nullable restore
#line 9 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Premi\_CreatePenyelesaianPremi.cshtml"
       Html.RenderPartial("_JadwalPembayaranPremi"); 

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\n<div class=\"position-relative row form-check\">\n    <div class=\"col-sm-9 offset-sm-2\">\n        <button class=\"btn btn-save pull-right\">Simpan</button>\n    </div>\n</div>\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "0bf6483c3c8fb259d34c15e6f7a0cee0ba14e8594986", async() => {
                WriteLiteral("Kembali");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\n");
#nullable restore
#line 18 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Premi\_CreatePenyelesaianPremi.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Epolis.Models.MTableNoPolis> Html { get; private set; }
    }
}
#pragma warning restore 1591
