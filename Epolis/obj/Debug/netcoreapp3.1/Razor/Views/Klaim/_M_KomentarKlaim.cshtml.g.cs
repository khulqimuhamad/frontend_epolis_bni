#pragma checksum "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Klaim\_M_KomentarKlaim.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cc82e357c7abce75c002cf6d38e010c7effa02e8"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Klaim__M_KomentarKlaim), @"mvc.1.0.view", @"/Views/Klaim/_M_KomentarKlaim.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cc82e357c7abce75c002cf6d38e010c7effa02e8", @"/Views/Klaim/_M_KomentarKlaim.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"91e3649ee4e062e36c4124dd594d5e54108a11d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Klaim__M_KomentarKlaim : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class=""modal-header"">
    <h5 class=""modal-title"" id=""exampleModalLongTitle"">Komentar Submit</h5>
    <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
        <span aria-hidden=""true"">&times;</span>
    </button>
</div>
<div class=""modal-body"">
    <div class=""col-md-12 row"">
        <div class=""input-group"">
            <textarea class=""form-control"" id=""komentarKlaim""></textarea>
        </div>
        <div id=""errorKomentarKlaim"" style=""color:red; display:none"">Harap mengisi komentar</div>
    </div>
</div>
<div class=""modal-footer"">
    <a class=""btn btn-white"" data-dismiss=""modal"" id=""closeSubModal"">Close</a>
    <a class=""btn btn-success"" href=""javascript:;"" onclick=""submitKomentarKlaim()"">Submit</a>
</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
