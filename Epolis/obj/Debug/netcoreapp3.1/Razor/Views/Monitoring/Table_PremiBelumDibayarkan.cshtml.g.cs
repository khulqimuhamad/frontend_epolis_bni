#pragma checksum "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "9f14ea641e4405ebb444328f56c03118dcb204f0"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Monitoring_Table_PremiBelumDibayarkan), @"mvc.1.0.view", @"/Views/Monitoring/Table_PremiBelumDibayarkan.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"9f14ea641e4405ebb444328f56c03118dcb204f0", @"/Views/Monitoring/Table_PremiBelumDibayarkan.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"91e3649ee4e062e36c4124dd594d5e54108a11d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Monitoring_Table_PremiBelumDibayarkan : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div id=""DivContentBody"">
    <div class=""row"" style=""padding-top:20px;"">
        <div class=""col-lg-12"">
            <div class=""main-card mb-3 card"">
                <div class=""headerSearch"">
                    Premi Belum Dibayarkan
                </div>
                <div class=""card-body"">
");
            WriteLiteral(@"                    <table id=""TablePremiBelumDibayarkan"" class=""display nowrap table table-bordered table-hover"" style=""width:100%"">
                        <thead>
                            <tr>
                                <th style=""max-width:25px"">No</th>
                                <th>Aksi</th>
                                <th>
                                    Periode Jatuh Tempo
                                </th>
                                <th>
                                    No Polis
                                </th>
                                <th>
                                    Nama Tertanggung
                                </th>
                                <th>
                                    TSI
                                </th>
                                <th>
                                    Premi Belum Dibayarkan
                                </th>
                                <th>
                          ");
            WriteLiteral(@"          Nama Asuradur
                                </th>
                                <th>
                                    Nama RM
                                </th>
                                <th>
                                    Alasan
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id=""DivFormBody"">
</div>

<style>
    .fab {
        position: fixed;
        bottom: 20px;
        right: 20px;
        clip-path: circle();
        height: 40px;
        width: 40px;
        z-index: 999;
        box-shadow: 0 0.46875rem 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03);
    }

    .addButton {
        position: fixed;
        bottom: 30px;
        righ");
            WriteLiteral(@"t: 30px;
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }

    .select2 {
        height: 30px;
    }

    .headerSearch {
        background-color: #3f6ad8;
        width: 50%;
        position: absolute;
        margin-top: -20px;
        margin-left: 20px;
        height: 35px;
        border-radius: .25rem;
        text-align: left;
        padding-left: 10px;
        padding-top: 5px;
        color: white;
    }

    .dataTables_wrapper {
        zoom: 0.9 !important;
    }

    table.dataTable.dtr-inline.collapsed > tbody > tr[role=""row""] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role=""row""] > th:first-child:before {
        background-color: #3f6ad8;
    }

    .select2-container .select2-selection--single {
        height: 35px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding-top: 5px;
        color: #495057;
        /*border: 1px solid #c");
            WriteLiteral("ed4da;*/\r\n    }\r\n\r\n    .input-group {\r\n        margin-bottom: 5px;\r\n    }\r\n\r\n    select[name=Table_length] {\r\n        height: 35px;\r\n    }\r\n</style>\r\n\r\n");
            DefineSection("styles", async() => {
                WriteLiteral("\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6570, "\"", 6649, 1);
#nullable restore
#line 153 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 6577, Url.Content("~/plugin/DataTables-1.10.20/media/css/datatables.min.css"), 6577, 72, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6681, "\"", 6771, 1);
#nullable restore
#line 154 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 6688, Url.Content("~/plugin/DataTables-1.10.20/media/css/responsive.dataTables.min.css"), 6688, 83, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6803, "\"", 6900, 1);
#nullable restore
#line 155 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 6810, Url.Content("~/plugin/DataTables-1.10.20/extensions/Select/css/select.bootstrap.min.css"), 6810, 90, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6932, "\"", 6996, 1);
#nullable restore
#line 156 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 6939, Url.Content("~/plugin/select2/dist/css/select2.min.css"), 6939, 57, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7028, "\"", 7105, 1);
#nullable restore
#line 157 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7035, Url.Content("~/plugin/bootstrap-daterangepicker/daterangepicker.css"), 7035, 70, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7137, "\"", 7228, 1);
#nullable restore
#line 158 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7144, Url.Content("~/plugin/jquery-smartwizard-master/dist/css/smart_wizard_all.min.css"), 7144, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7260, "\"", 7321, 1);
#nullable restore
#line 159 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7267, Url.Content("~/richtexteditor/rte_theme_default.css"), 7267, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n");
            }
            );
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7377, "\"", 7460, 1);
#nullable restore
#line 162 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7383, Url.Content("~/plugin/DataTables-1.10.20/media/js/jquery.dataTables.min.js"), 7383, 77, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7484, "\"", 7571, 1);
#nullable restore
#line 163 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7490, Url.Content("~/plugin/DataTables-1.10.20/media/js/dataTables.responsive.min.js"), 7490, 81, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7595, "\"", 7690, 1);
#nullable restore
#line 164 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7601, Url.Content("~/plugin/DataTables-1.10.20/extensions/Select/js/dataTables.select.min.js"), 7601, 89, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7714, "\"", 7804, 1);
#nullable restore
#line 165 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7720, Url.Content("~/plugin/jquery-smartwizard-master/dist/js/jquery.smartWizard.min.js"), 7720, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7828, "\"", 7889, 1);
#nullable restore
#line 166 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7834, Url.Content("~/plugin/select2/dist/js/select2.min.js"), 7834, 55, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7913, "\"", 7991, 1);
#nullable restore
#line 167 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 7919, Url.Content("~/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"), 7919, 72, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8015, "\"", 8060, 1);
#nullable restore
#line 168 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 8021, Url.Content("~/richtexteditor/rte.js"), 8021, 39, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"text/javascript\"></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8107, "\"", 8168, 1);
#nullable restore
#line 169 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
WriteAttributeValue("", 8113, Url.Content("~/richtexteditor/plugins/all_plugins.js"), 8113, 55, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"text/javascript\"></script>\r\n    ");
#nullable restore
#line 170 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_PremiBelumDibayarkan.cshtml"
Write(Html.Partial("_Script"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
