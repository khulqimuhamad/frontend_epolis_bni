#pragma checksum "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "aa66d9c2d582adf23eecdcdf2ae1d80d74de5882"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Monitoring_Table_OutstandingPremi), @"mvc.1.0.view", @"/Views/Monitoring/Table_OutstandingPremi.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\_ViewImports.cshtml"
using Epolis.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"aa66d9c2d582adf23eecdcdf2ae1d80d74de5882", @"/Views/Monitoring/Table_OutstandingPremi.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"91e3649ee4e062e36c4124dd594d5e54108a11d2", @"/Views/_ViewImports.cshtml")]
    public class Views_Monitoring_Table_OutstandingPremi : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div id=""DivContentBody"">
    <div class=""row"" style=""padding-top:20px;"">
        <div class=""col-lg-12"">
            <div class=""main-card mb-3 card"">
                <div class=""headerSearch"">
                    Outstanding Premi
                </div>
                <div class=""card-body"">
");
            WriteLiteral(@"                    <table id=""TableOutstandingPremi"" class=""display nowrap table table-bordered table-hover"" style=""width:100%"">
                        <thead>
                            <tr>
                                <th style=""max-width:25px"">No</th>
                                <th>
                                    Tanggal Polis
                                </th>
                                <th>
                                    Nama Asuradur
                                </th>
                                <th>
                                    No Polis
                                </th>
                                <th>
                                    Nama Debitur
                                </th>
                                <th>
                                    Tagihan Premi
                                </th>
                                <th>
                                    Tagihan Yang Sudah Dibayar
                      ");
            WriteLiteral(@"          </th>
                                <th>
                                    Tagihan Yang Belum Dibayar
                                </th>
                                <th>
                                    Tanggal Sudah Bayar
                                </th>
                                <th>
                                    Nama RM
                                </th>
                                <th>
                                    Status dari Asuradur
                                </th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id=""DivFormBody"">
</div>

<style>
    .fab {
        position: fixed;
        bottom: 20px;
        right: 20px;
        clip-path: circle();
        height: 40px;
        width: 40px;
        z-index: 999;
        box-shadow: 0 0.46875re");
            WriteLiteral(@"m 2.1875rem rgba(4,9,20,0.03), 0 0.9375rem 1.40625rem rgba(4,9,20,0.03), 0 0.25rem 0.53125rem rgba(4,9,20,0.05), 0 0.125rem 0.1875rem rgba(4,9,20,0.03);
    }

    .addButton {
        position: fixed;
        bottom: 30px;
        right: 30px;
        border-radius: 50%;
        width: 50px;
        height: 50px;
    }

    .select2 {
        height: 30px;
    }

    .headerSearch {
        background-color: #3f6ad8;
        width: 50%;
        position: absolute;
        margin-top: -20px;
        margin-left: 20px;
        height: 35px;
        border-radius: .25rem;
        text-align: left;
        padding-left: 10px;
        padding-top: 5px;
        color: white;
    }

    .dataTables_wrapper {
        zoom: 0.9 !important;
    }

    table.dataTable.dtr-inline.collapsed > tbody > tr[role=""row""] > td:first-child:before, table.dataTable.dtr-inline.collapsed > tbody > tr[role=""row""] > th:first-child:before {
        background-color: #3f6ad8;
    }

    .select2-con");
            WriteLiteral(@"tainer .select2-selection--single {
        height: 35px;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding-top: 5px;
        color: #495057;
        /*border: 1px solid #ced4da;*/
    }

    .input-group {
        margin-bottom: 5px;
    }

    select[name=Table_length] {
        height: 35px;
    }
</style>

");
            DefineSection("styles", async() => {
                WriteLiteral("\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6807, "\"", 6886, 1);
#nullable restore
#line 158 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 6814, Url.Content("~/plugin/DataTables-1.10.20/media/css/datatables.min.css"), 6814, 72, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 6918, "\"", 7008, 1);
#nullable restore
#line 159 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 6925, Url.Content("~/plugin/DataTables-1.10.20/media/css/responsive.dataTables.min.css"), 6925, 83, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7040, "\"", 7137, 1);
#nullable restore
#line 160 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7047, Url.Content("~/plugin/DataTables-1.10.20/extensions/Select/css/select.bootstrap.min.css"), 7047, 90, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7169, "\"", 7233, 1);
#nullable restore
#line 161 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7176, Url.Content("~/plugin/select2/dist/css/select2.min.css"), 7176, 57, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7265, "\"", 7342, 1);
#nullable restore
#line 162 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7272, Url.Content("~/plugin/bootstrap-daterangepicker/daterangepicker.css"), 7272, 70, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7374, "\"", 7465, 1);
#nullable restore
#line 163 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7381, Url.Content("~/plugin/jquery-smartwizard-master/dist/css/smart_wizard_all.min.css"), 7381, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n    <link");
                BeginWriteAttribute("href", " href=\"", 7497, "\"", 7558, 1);
#nullable restore
#line 164 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7504, Url.Content("~/richtexteditor/rte_theme_default.css"), 7504, 54, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" rel=\"stylesheet\" />\r\n");
            }
            );
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7614, "\"", 7697, 1);
#nullable restore
#line 167 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7620, Url.Content("~/plugin/DataTables-1.10.20/media/js/jquery.dataTables.min.js"), 7620, 77, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7721, "\"", 7808, 1);
#nullable restore
#line 168 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7727, Url.Content("~/plugin/DataTables-1.10.20/media/js/dataTables.responsive.min.js"), 7727, 81, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7832, "\"", 7927, 1);
#nullable restore
#line 169 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7838, Url.Content("~/plugin/DataTables-1.10.20/extensions/Select/js/dataTables.select.min.js"), 7838, 89, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 7951, "\"", 8041, 1);
#nullable restore
#line 170 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 7957, Url.Content("~/plugin/jquery-smartwizard-master/dist/js/jquery.smartWizard.min.js"), 7957, 84, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8065, "\"", 8126, 1);
#nullable restore
#line 171 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 8071, Url.Content("~/plugin/select2/dist/js/select2.min.js"), 8071, 55, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8150, "\"", 8228, 1);
#nullable restore
#line 172 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 8156, Url.Content("~/plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"), 8156, 72, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral("></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8252, "\"", 8297, 1);
#nullable restore
#line 173 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 8258, Url.Content("~/richtexteditor/rte.js"), 8258, 39, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"text/javascript\"></script>\r\n    <script");
                BeginWriteAttribute("src", " src=\"", 8344, "\"", 8405, 1);
#nullable restore
#line 174 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
WriteAttributeValue("", 8350, Url.Content("~/richtexteditor/plugins/all_plugins.js"), 8350, 55, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(" type=\"text/javascript\"></script>\r\n    ");
#nullable restore
#line 175 "D:\Portal BNI E-Polis Merge BNI\Portal BNI E-Polis\Epolis\Views\Monitoring\Table_OutstandingPremi.cshtml"
Write(Html.Partial("_Script"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\r\n\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
