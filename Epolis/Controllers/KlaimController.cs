﻿using Epolis.Component;
using Epolis.Models;
using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Controllers
{
    public class KlaimController : ParentController
    {
        private readonly EpolisContext _context;

        public KlaimController(EpolisContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }
        #region Load Table Data Klaim Kerugian
        public IActionResult LoadTableKlaim(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_KlaimKerugian>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //var TypeSearchParam = dict["columns[2][search][value]"];
                    //var NameSearchParam = dict["columns[3][search][value]"];

                    //model.TypeSearch = TypeSearchParam;
                    //model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    //model.isEndorsement = 1;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_KlaimKerugian>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0});

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_KlaimKerugian>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Data Penutupan
        public IActionResult LoadTablePenutupan(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_KlaimKerugian>();
                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //var TypeSearchParam = dict["columns[2][search][value]"];
                    //var NameSearchParam = dict["columns[3][search][value]"];

                    //model.TypeSearch = TypeSearchParam;
                    //model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    //model.isEndorsement = 0;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getPenutupan"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_KlaimKerugian>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data= new List<M_KlaimKerugian>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Data Objek Pertanggungan
        public IActionResult LoadTableObjekPertanggungan(string id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_ObjekPertanggungan>();
                LoadVM model = new LoadVM();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    model.id = id;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getObjekPertanggungan"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_ObjekPertanggungan>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_ObjekPertanggungan>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data});
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table File Pendukung
        public IActionResult LoadTableFilePendukung(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_FilePendukung>();
                LoadVM model = new LoadVM();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    model.id = id.ToString();


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:loadFilePendukungKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_FilePendukung>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                       
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_FilePendukung>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion

        #region Modal Detail Objek Pertanggungan
        public IActionResult M_DetailObjekPertanggungan_Klaim(int Id_Objek)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            var data = new ServiceResult<M_KlaimObjekPertanggungan>();

            try
            {
                EditVM model = new EditVM()
                {
                    id = Id_Objek
                };

                //M_KlaimObjekPertanggungan model = new M_KlaimObjekPertanggungan()
                //{
                //    IdObjekPenutupan = Id_Objek
                //};
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getById_KlaimObjek"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<M_KlaimObjekPertanggungan>>(result);
                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");

                }
                else
                {
                    //if (data.Data == null)
                    //{
                    //    M_KlaimObjekPertanggungan modelCreateKlaimObjek = new M_KlaimObjekPertanggungan()
                    //    {
                    //        IdObjekPenutupan = Id_Objek
                    //    };
                    //    return PartialView("_M_Klaim_ObjekPertanggungan",data);
                    //}
                    return PartialView("_M_Klaim_ObjekPertanggungan",data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");

            }
            //return PartialView("_M_Klaim_ObjekPertanggungan");

            //return PartialView("_M_Detail_ObjekPertanggungan");
        }
        #endregion
        #region Modal Submit Komentar Klaim 
        public IActionResult M_Klaim_KomentarKlaim(int Id_Objek)
        {
            return PartialView("_M_KomentarKlaim");

        }
        #endregion
        #region Create Klaim Kerugian
        public IActionResult Create_KlaimKerugian()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            return PartialView("_Create_KlaimKerugian");
        }
        #endregion
        #region Edit Klaim Kerugian
        public IActionResult EditKlaim(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            var data = new ServiceResult<M_KlaimKerugian>();
            try
            {
                EditVM model = new EditVM()
                {
                    id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getByIdKlaim"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<M_KlaimKerugian>>(result);
                if (data.Data.Id_Approver.ToString() != "0")
                {
                    ViewBag.Id_Approver = new SelectList(SelectDataApprover(data.Data.Id_Approver.ToString(), _context), "id", "text", data.Data.Id_Approver);
                }
                else
                {
                    ViewBag.Id_Approver = new SelectList("", "");
                }

                if (data.Data.Id_Reviewer.ToString() != "0")
                {
                    ViewBag.Id_Reviewer = new SelectList(SelectDataReviewer(data.Data.Id_Reviewer.ToString(), _context), "id", "text", data.Data.Id_Reviewer);
                }
                else
                {
                    ViewBag.Id_Reviewer = new SelectList("", "");
                }
                if (data.Data.Id_InputerADK.ToString() != "0")
                {
                    ViewBag.Id_InputerADK = new SelectList(SelectDataInputerADK(data.Data.Id_InputerADK.ToString(), _context), "id", "text", data.Data.Id_InputerADK);
                }
                else
                {
                    ViewBag.IdInputerADK = new SelectList("", "");
                }

                if (data.Data.IdRMPengelola.ToString() != "0")
                {
                    ViewBag.IdRMPengelola = new SelectList(SelectDataRMPengelola(data.Data.IdRMPengelola.ToString(), _context), "id", "text", data.Data.IdRMPengelola);
                }
                else
                {
                    ViewBag.IdRMPengelola = new SelectList("", "");
                }

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("_Edit_KlaimKerugian", data.Data);
                }
            }
            catch (Exception ex)
            {
                return View(ex.Message);

            }


            //return PartialView("_Edit_KlaimKerugian");
        }
        #endregion
        #region Create Upload Data Pendukung
        public IActionResult M_UploadDataPendukung(int Id_Objek)
        {
            return PartialView("_M_UploadDataPendukung");

        }
        #endregion

        public IActionResult IndexProses()
        {
            return View();
        }
        #region Load Table Data Klaim Kerugian Proses
        public IActionResult LoadTableKlaimProses(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_KlaimKerugian>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //var TypeSearchParam = dict["columns[2][search][value]"];
                    //var NameSearchParam = dict["columns[3][search][value]"];

                    //model.TypeSearch = TypeSearchParam;
                    //model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    //model.isEndorsement = 1;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_KlaimKerugian>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_KlaimKerugian>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Data Penutupan Proses
        public IActionResult LoadTablePenutupanProses(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_KlaimKerugian>();
                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //var TypeSearchParam = dict["columns[2][search][value]"];
                    //var NameSearchParam = dict["columns[3][search][value]"];

                    //model.TypeSearch = TypeSearchParam;
                    //model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    //model.isEndorsement = 0;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getPenutupan"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_KlaimKerugian>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_KlaimKerugian>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Data Objek Pertanggungan Proses
        public IActionResult LoadTableObjekPertanggunganProses(string id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_ObjekPertanggungan>();
                LoadVM model = new LoadVM();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    model.id = id;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getObjekPertanggungan"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_ObjekPertanggungan>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_ObjekPertanggungan>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table File Pendukung Proses
        public IActionResult LoadTableFilePendukungProses(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_FilePendukung>();
                LoadVM model = new LoadVM();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    model.id = id.ToString();


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:loadFilePendukungKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_FilePendukung>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");

                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_FilePendukung>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table History
        public IActionResult LoadTableHistory(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_THistory>();
                LoadVM model = new LoadVM();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    model.id = id.ToString();


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getHistory"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_THistory>>(result);

                    if (data.code == -2)
                    {
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_THistory>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion

        #region View Klaim Proses
        public ActionResult ViewProses(int id)
        {
                if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<M_KlaimKerugian>();
            try
            {
                EditVM model = new EditVM()
                {
                    id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getByIdKlaim"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<M_KlaimKerugian>>(result);


                if (data.Data.Id_Approver.ToString() != "0")
                {
                    ViewBag.Id_Approver = new SelectList(SelectDataApprover(data.Data.Id_Approver.ToString(), _context), "id", "text", data.Data.Id_Approver);
                }
                else
                {
                    ViewBag.Id_Approver = new SelectList("", "");
                }

                if (data.Data.Id_Reviewer.ToString() != "0")
                {
                    ViewBag.Id_Reviewer = new SelectList(SelectDataReviewer(data.Data.Id_Reviewer.ToString(), _context), "id", "text", data.Data.Id_Reviewer);
                }
                else
                {
                    ViewBag.Id_Reviewer = new SelectList("", "");
                }

                if (data.Data.Id_InputerADK.ToString() != "0" )
                {
                    ViewBag.Id_InputerADK = new SelectList(SelectDataInputerADK(data.Data.Id_InputerADK.ToString(), _context), "id", "text", data.Data.Id_InputerADK);
                }
                else
                {
                    ViewBag.IdInputerADK = new SelectList("", "");
                }
                if (HttpContext.Session.GetString("USERGROUPCODE") == "REV" || HttpContext.Session.GetString("USERGROUPCODE") == "APP")
                {
                    if (data.Data.Id_ReviewerADK.ToString() != "0")
                    {
                        ViewBag.Id_ReviewerADK = new SelectList(SelectDataReviewerADK(data.Data.Id_ReviewerADK.ToString(), _context), "id", "text", data.Data.Id_ReviewerADK);
                    }
                    else
                    {
                        ViewBag.Id_ReviewerADK = new SelectList("", "");
                    }

                    if (data.Data.Id_ApproverADK.ToString() != "0")
                    {
                        ViewBag.Id_ApproverADK = new SelectList(SelectDataApproverADK(data.Data.Id_ApproverADK.ToString(), _context), "id", "text", data.Data.Id_ApproverADK);
                    }
                    else
                    {
                        ViewBag.Id_ApproverADK = new SelectList("", "");
                    }

                }

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("_ViewKlaimKerugian_Proses", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
        #region View Detail Objek Pertanggungan Proses
        public IActionResult View_DetailObjekPertanggungan_Klaim(int Id_Objek)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            var data = new ServiceResult<M_KlaimObjekPertanggungan>();

            try
            {
                EditVM model = new EditVM()
                {
                    id = Id_Objek
                };

                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getById_KlaimObjek"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<M_KlaimObjekPertanggungan>>(result);
                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");

                }
                else
                {
                    return PartialView("_View_M_Klaim_ObjekPertanggungan_Proses", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");

            }
            
        }
        #endregion
    }
}
