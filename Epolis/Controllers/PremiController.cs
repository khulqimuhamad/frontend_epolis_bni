﻿using Epolis.Models;
using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Epolis.Controllers
{
    [ResponseCache(Location = ResponseCacheLocation.None, NoStore = true)]
    public class PremiController : Controller
    {
        private readonly EpolisContext _context;

        public PremiController(EpolisContext context)
        {
            _context = context;
        }
        //public ActionResult Index()
        //{
        //    var lookup = from t in _context.Mlookup
        //                 where t.ISDELETED != true
        //                 select t;

        //    return View(lookup);
        //}

        public IActionResult Index(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModel();

                try
                {
                    model.Column = "*";
                    model.Filter = "0=0";
                    model.Orderby = "ID";
                    model.Firstrow = 0;
                    model.Secondrow = 10;
                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:get"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                    List<TPremi> listMlookup;
                    using (var textReader = new StringReader(data.data.data.ToString()))
                    {
                        using (var reader = new JsonTextReader(textReader))
                        {
                            listMlookup = new JsonSerializer().Deserialize(reader, typeof(List<TPremi>)) as List<TPremi>;
                        }
                    }
                    ViewData["listMlookup"] = listMlookup;
                    return View(listMlookup);

                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }

        public IActionResult IndexIkhtisarPertanggungan(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModel();

                try
                {
                    model.Column = "*";
                    model.Filter = "0=0";
                    model.Orderby = "ID";
                    model.Firstrow = 0;
                    model.Secondrow = 10;
                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:get"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModel>(result);
                    List<TPremi> listMlookup;
                    using (var textReader = new StringReader(data.data.data.ToString()))
                    {
                        using (var reader = new JsonTextReader(textReader))
                        {
                            listMlookup = new JsonSerializer().Deserialize(reader, typeof(List<TPremi>)) as List<TPremi>;
                        }
                    }
                    ViewData["listMlookup"] = listMlookup;
                    return View(listMlookup);

                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }

        public IActionResult LoadTableTagihanPremi(LoadTagihanPremiVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableListViewModels<MTagihanPremi>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    //model.TypeSearch = 
                    //model.NameSearch = 
                    //model.sortColumn = "";
                    //model.sortColumnDir = "";
                    model.PageNumber = pageNumber;
                    model.RowsPage = pageSize;

                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MTagihanPremi:get"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTableListViewModels<MTagihanPremi>>(result);

                    if (data.code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        //if (data.data == null)
                        //{
                        //    data.data = new DataTableListViewModels<MTagihanPremi>();
                        //    recordsTotal = 0;
                        //}

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }

        public IActionResult LoadTablePolis(LoadTagihanPremiVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableListViewModels<MTableNoPolis>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    //model.TypeSearch = 
                    //model.NameSearch = 
                    //model.sortColumn = "";
                    //model.sortColumnDir = "";
                    model.PageNumber = pageNumber;
                    model.RowsPage = pageSize;

                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MTagihanPremi:get"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTableListViewModels<MTableNoPolis>>(result);

                    if (data.code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        //if (data.data == null)
                        //{
                        //    data.data = new DataTableListViewModels<MTagihanPremi>();
                        //    recordsTotal = 0;
                        //}

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }

        public IActionResult LoadTablePenyelesaianPremi(LoadTagihanPremiVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableListViewModels<MTablePenyelesaianPremi>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;

                    //model.TypeSearch = 
                    //model.NameSearch = 
                    //model.sortColumn = "";
                    //model.sortColumnDir = "";
                    model.PageNumber = pageNumber;
                    model.RowsPage = pageSize;

                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MPenyelesaianPremi:get"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = Newtonsoft.Json.JsonConvert.DeserializeObject<DataTableListViewModels<MTablePenyelesaianPremi>>(result);

                    if (data.code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        //if (data.data == null)
                        //{
                        //    data.data = new DataTableListViewModels<MTagihanPremi>();
                        //    recordsTotal = 0;
                        //}

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        public IActionResult ViewPerincianTagihan(int? id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MTagihanPremi:getById"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id }, token);
                var jsonResult = JsonConvert.DeserializeObject<ResponseViewModel<MTagihanPremi>>(result);

                return PartialView("_ViewPerincianTagihan", jsonResult.data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        public IActionResult ViewPenyelesaianPremi(int? id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MPenyelesaianPremi:getById"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id }, token);
                var jsonResult = JsonConvert.DeserializeObject<ResponseViewModel<MTablePenyelesaianPremi>>(result);

                return PartialView("_ViewPenyelesaianPremi", jsonResult.data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        public IActionResult CreatePencatatanPremi()
        {
            //if (!lastSession.Update())
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            //ViewBag.DropdownRole = new SelectList(SelectLookupRole("EmployeeRole"), "Value", "Name");
            //ViewBag.DropdownUnit = new SelectList(SelectUnit(), "Code", "Name");
            //ViewBag.DropdownStatusRole = new SelectList(SelectLookup("StatusRole"), "Value", "Name");

            return PartialView("_CreatePencatatanPremi");
        }

        public IActionResult CreateJadwalPremi()
        {
            //if (!lastSession.Update())
            //{
            //    return RedirectToAction("Login", "Account");
            //}

            //ViewBag.DropdownRole = new SelectList(SelectLookupRole("EmployeeRole"), "Value", "Name");
            //ViewBag.DropdownUnit = new SelectList(SelectUnit(), "Code", "Name");
            //ViewBag.DropdownStatusRole = new SelectList(SelectLookup("StatusRole"), "Value", "Name");

            return PartialView("_CreateJadwalPremi");
        }

        public IActionResult CreatePenyelesaianPremi()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            return PartialView("_CreatePenyelesaianPremi");
        }
        // GET: Lookup/Create
        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            return View();
        }

        public IActionResult CreatePenyelesaian()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            return View();
        }

        public IActionResult ViewBelum()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            return View();
        }

        // POST: Lookup/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TPremi model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:create"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                if (resultApi && !string.IsNullOrEmpty(result))
                    return RedirectToAction("Index");
                else
                    return Content("Failed Save");
            }
            catch (Exception ex)
            {
                ViewBag.MessageError = ex.Message;
            }
            ViewBag.EnableErrorSummary = true;
            return View();
        }

        //public async Task<IActionResult> Create([Bind("ID,TYPE,NAME,VALUE,ORDERBY,UPDATEDBYID,UPDATEDTIME,ISDELETED,DELETEDBYID,DELETEDTIME,CREATEDTIME,CREATEDBYID,ISACTIVE")] Mlookup mlookup)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(mlookup);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(mlookup);
        //}

        // GET: Lookup/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var mlookup = await _context.Mlookup.FindAsync(id);
        //    if (mlookup == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(mlookup);
        //}
        public IActionResult Edit(int? id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:getById"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id }, token);
                var jsonResult = JsonConvert.DeserializeObject<ResponseViewModel<TPremi>>(result);

                return View("Edit", jsonResult.data);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: Lookup/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(TPremi model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                //if (model.ISACTIVE == true) ;
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:edit"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                if (resultApi && !string.IsNullOrEmpty(result))
                    return RedirectToAction("Index");
                else
                    return Content("Failed Update");
            }
            catch (Exception ex)
            {
                ViewBag.MessageError = ex.Message;
            }
            ViewBag.EnableErrorSummary = true;
            return View("Index", model);
        }
        public IActionResult Delete(int? id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            try
            {
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mlookup:delete"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, new { id }, token);
                var jsonResult = JsonConvert.DeserializeObject<ResponseViewModel<TPremi>>(result);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        private bool MlookupExists(int id)
        {
            return _context.TPremi.Any(e => e.ID == id);
        }
    }
}
