﻿using Epolis.Models;
using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Controllers
{
    public class TrackingKlaimController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        #region Load Table Data Klaim Kerugian Proses
        public IActionResult LoadTableTracking(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModelList<M_KlaimKerugian>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    //var TypeSearchParam = dict["columns[2][search][value]"];
                    //var NameSearchParam = dict["columns[3][search][value]"];

                    //model.TypeSearch = TypeSearchParam;
                    //model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;
                    //model.isEndorsement = 1;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:getKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModelList<M_KlaimKerugian>>(result);

                    if (data.code == -2)
                    {
                        //HttpContext.Session.Clear();
                        //return RedirectToAction("Login", "Login");
                        return Json(new { draw = draw, recordsFiltered = 0, recordsTotal = 0, data = 0 });

                    }
                    else
                    {
                        if (data.data.data == null)
                        {
                            data.data.data = new List<M_KlaimKerugian>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.data.recordTotals, recordsTotal = data.data.recordTotals, data = data.data.data });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
    }
}
