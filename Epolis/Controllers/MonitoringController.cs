﻿using Epolis.Models;
using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Controllers
{
    public class MonitoringController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        #region Load Table Obyek Pertanggungan Belum Diasuransikan
        public IActionResult LoadTableObyekPertanggunganBelumDiasuransikan(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MobyekPertanggunganBelumDiasuransikan>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var TypeSearchParam = dict["columns[2][search][value]"];
                    var NameSearchParam = dict["columns[3][search][value]"];

                    model.TypeSearch = TypeSearchParam;
                    model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:getObyek"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MobyekPertanggunganBelumDiasuransikan>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MobyekPertanggunganBelumDiasuransikan>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Daftar Polis Jatuh Tempo Yang Telah Diperpanjang Otomatis
        public IActionResult LoadTableDaftarPolisJatuhTempoYangTelahDiperpanjangOtomatis(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MdaftarPolisJatuhTempoYangTelahDiperpanjangOtomatis>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var TypeSearchParam = dict["columns[2][search][value]"];
                    var NameSearchParam = dict["columns[3][search][value]"];

                    model.TypeSearch = TypeSearchParam;
                    model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:getPolis"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MdaftarPolisJatuhTempoYangTelahDiperpanjangOtomatis>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MdaftarPolisJatuhTempoYangTelahDiperpanjangOtomatis>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Outstanding Premi
        public IActionResult LoadDataTableOutstandingPremi(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MoutstandingPremi>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var TypeSearchParam = dict["columns[2][search][value]"];
                    var NameSearchParam = dict["columns[3][search][value]"];

                    model.TypeSearch = TypeSearchParam;
                    model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:getOutstanding"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MoutstandingPremi>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MoutstandingPremi>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Premi Telah Lunas
        public IActionResult LoadDataTablePremiTelahLunas(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MpremiTelahLunas>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var TypeSearchParam = dict["columns[2][search][value]"];
                    var NameSearchParam = dict["columns[3][search][value]"];

                    model.TypeSearch = TypeSearchParam;
                    model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:getPremiLunas"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MpremiTelahLunas>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MpremiTelahLunas>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Premi Belum Dibayarkan
        public IActionResult LoadDataTablePremiBelumDibayarkan(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MpremiBelumDibayarkan>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var TypeSearchParam = dict["columns[2][search][value]"];
                    var NameSearchParam = dict["columns[3][search][value]"];

                    model.TypeSearch = TypeSearchParam;
                    model.NameSearch = NameSearchParam;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:getPremiBelumLunas"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MpremiBelumDibayarkan>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MpremiBelumDibayarkan>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion

        #region View Obyek Pertanggungan Belum Diasuransikan
        public ActionResult ViewLoadTableObyekPertanggunganBelumDiasuransikan(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MobyekPertanggunganBelumDiasuransikan>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:viewObyek"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MobyekPertanggunganBelumDiasuransikan>>(result);

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewObyekPertanggunganBelumDiasuransikan", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
        #region View Premi Belum Dibayarkasn
        public ActionResult ViewPremiBelumDibayarkasn(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MpremiBelumDibayarkan>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mmonitoring:viewPremiBelumLunas"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MpremiBelumDibayarkan>>(result);

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewPremiBelumDibayarkan", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion



    }
}
