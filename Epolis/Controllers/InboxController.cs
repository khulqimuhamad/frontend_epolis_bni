﻿using Epolis.Component;
using Epolis.Models;
using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Controllers
{
    public class InboxController : ParentController
    {
        private readonly EpolisContext _context;

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult IndexProses()
        {
            return View();
        }

        #region Load Table Respon Klaim Disetujui
        public IActionResult LoadDataTableRespoinKlaimDisetujui(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MresponKlaim>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;    
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var cif = dict["columns[2][search][value]"];
                    var status = dict["columns[3][search][value]"];
                    var namaDebitur = dict["columns[4][search][value]"];
                    var namaRm = dict["columns[5][search][value]"];

                    model.cif = cif;
                    model.status = 10;
                    model.NamaDebitur = namaDebitur;
                    model.NamaRM = namaRm;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:getResponKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MresponKlaim>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MresponKlaim>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Respon Klaim DiTolak
        public IActionResult LoadDataTableRespoinKlaimDiTolak(LoadVM model)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MresponKlaim>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var cif = dict["columns[2][search][value]"];
                    var status = dict["columns[3][search][value]"];
                    var namaDebitur = dict["columns[4][search][value]"];
                    var namaRm = dict["columns[5][search][value]"];

                    model.cif = cif;
                    model.status = 11;
                    model.NamaDebitur = namaDebitur;
                    model.NamaRM = namaRm;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;


                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:getResponKlaim"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MresponKlaim>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MresponKlaim>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion
        #region Load Table Uraian Obyek Pertanggungan
        public IActionResult LoadDataTableUraianObyekPertanggungan(LoadVMint model, int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }
            {
                var data = new DataTableViewModels<MuraianObyekPertanggungan>();

                try
                {
                    var dict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
                    var draw = dict["draw"];
                    var start = dict["start"];
                    var length = dict["length"];
                    int recordsTotal = 0;
                    var pageNumber = (int.Parse(start) / int.Parse(length)) + 1;
                    var sortColumn = dict["columns[" + dict["order[0][column]"] + "][data]"];
                    var sortColumnDir = dict["order[0][dir]"];
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    var search = dict["columns[2][search][value]"];

                    model.id = id;
                    model.SortCoulumn = sortColumn;
                    model.SortCoulumnDir = sortColumnDir;
                    model.PageNumber = pageNumber;
                    model.RowPage = pageSize;

                    var token = HttpContext.Session.GetString("TOKEN");
                    var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:getUraianObyek"];
                    (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                    if (resultApi && !string.IsNullOrEmpty(result))
                        data = JsonConvert.DeserializeObject<DataTableViewModels<MuraianObyekPertanggungan>>(result);

                    if (data.Code == -2)
                    {
                        HttpContext.Session.Clear();
                        return RedirectToAction("Login", "Login");
                    }
                    else
                    {
                        if (data.list == null)
                        {
                            data.list = new List<MuraianObyekPertanggungan>();
                            recordsTotal = 0;
                        }

                        return Json(new { draw = draw, recordsFiltered = data.recordTotals, recordsTotal = data.recordTotals, data = data.list });
                    }
                }
                catch (Exception ex)
                {
                    return View(ex.Message);
                }
            }
        }
        #endregion

        #region Create Validasi Diterima
        public IActionResult CreateValidasiDiTerima()
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            return PartialView("ViewValidasiBNIdiTerima");
        }
        #endregion

        #region View Respon Klaim Disetujui
        public ActionResult ViewResponKlaimDisetujui(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MviewClaimAsuradur>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:viewResponKlaim"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MviewClaimAsuradur>>(result);

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewKlaimAsuradurDiSetujui", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
        #region View Respon Klaim Ditolak
        public ActionResult ViewResponKlaimDitolak(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MviewClaimAsuradur>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:viewResponKlaim"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MviewClaimAsuradur>>(result);

                if (data.Data.Id_ApproverUB.ToString() != "0")
                {
                    ViewBag.Id_ApproverUB = new SelectList(SelectDataApprover(data.Data.Id_ApproverUB.ToString(), _context), "id", "text", data.Data.Id_ApproverUB);
                }
                else
                {
                    ViewBag.Id_ApproverUB = new SelectList("", "");
                }

                if (data.Data.Id_ReviewerUB.ToString() != "0")
                {
                    ViewBag.Id_ReviewerUB = new SelectList(SelectDataReviewer(data.Data.Id_ReviewerUB.ToString(), _context), "id", "text", data.Data.Id_ReviewerUB);
                }
                else
                {
                    ViewBag.Id_ReviewerUB = new SelectList("", "");
                }

                if (data.Data.Id_InputerADK.ToString() != "0")
                {
                    ViewBag.Id_InputerADK = new SelectList(SelectDataInputerADK(data.Data.Id_InputerADK.ToString(), _context), "id", "text", data.Data.Id_InputerADK);
                }
                else
                {
                    ViewBag.Id_InputerADK = new SelectList("", "");
                }

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewKlaimAsuradurDiTolak", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
        #region View Uraian Obyek Pertanggungan Disetujui
        public ActionResult ViewUraianObyekPertanggunganDisetujui(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MuraianObyekPertanggungan>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:viewUraianObyek"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MuraianObyekPertanggungan>>(result);

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewUraianObyekPertanggunganDiSetujui", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
        #region View Uraian Obyek Pertanggungan Ditolak
        public ActionResult ViewUraianObyekPertanggunganDitolak(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MuraianObyekPertanggungan>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:viewUraianObyek"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MuraianObyekPertanggungan>>(result);

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewUraianObyekPertanggunganDiTolak", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion

        #region View Respon Klaim Ditolak Proses
        public ActionResult ViewResponKlaimDitolakProses(int id)
        {
            if (HttpContext.Session.GetString("USERNAME") == null)
            {
                return RedirectToAction("LoginCheck", "Login");
            }

            var data = new ServiceResult<MviewClaimAsuradur>();
            try
            {
                EditVM model = new EditVM()
                {
                    Id = id
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:MInboxKlaim:viewResponKlaim"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);

                if (resultApi && !string.IsNullOrEmpty(result))
                    data = JsonConvert.DeserializeObject<ServiceResult<MviewClaimAsuradur>>(result);

                if (data.Data.Id_ApproverUB.ToString() != "0")
                {
                    ViewBag.Id_ApproverUB = new SelectList(SelectDataApprover(data.Data.Id_ApproverUB.ToString(), _context), "id", "text", data.Data.Id_ApproverUB);
                }
                else
                {
                    ViewBag.Id_ApproverUB = new SelectList("", "");
                }

                if (data.Data.Id_ReviewerUB.ToString() != "0")
                {
                    ViewBag.Id_ReviewerUB = new SelectList(SelectDataReviewer(data.Data.Id_ReviewerUB.ToString(), _context), "id", "text", data.Data.Id_ReviewerUB);
                }
                else
                {
                    ViewBag.Id_ReviewerUB = new SelectList("", "");
                }

                if (data.Data.Id_InputerADK.ToString() != "0")
                {
                    ViewBag.Id_InputerADK = new SelectList(SelectDataInputerADK(data.Data.Id_InputerADK.ToString(), _context), "id", "text", data.Data.Id_InputerADK);
                }
                else
                {
                    ViewBag.Id_InputerADK = new SelectList("", "");
                }

                if (data.Code == -2)
                {
                    HttpContext.Session.Clear();
                    return RedirectToAction("Login", "Account");
                }
                else
                {
                    return PartialView("ViewKlaimAsuradurDiTolakProses", data.Data);
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
        #endregion
    }
}
