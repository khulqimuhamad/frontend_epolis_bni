﻿using Epolis.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.Controllers
{
    public class UtilityController : Controller
    {

        #region Get All Dropdown Approver
        public JsonResult GetAllApprover(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel() 
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:approver"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get All Dropdown Reviewer
        public JsonResult GetAllReviewer(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:reviewer"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get All Dropdown InputerADK
        public JsonResult GetAllInputerADK(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:inputerADK"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get All Dropdown ReviewerADK
        public JsonResult GetAllReviewerADK(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:DropdownReviewerADK"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get All Dropdown ApproverADK
        public JsonResult GetAllApproverADK(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:DropdownApproverADK"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get All Dropdown RMPengelola
        public JsonResult GetAllRMPengelola(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:Mklaim:DropdownApproverADK"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion



        #region Get All Dropdown Kelas Konstruksi
        public JsonResult GetAllKelasKonstruksi(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:kelasKonstruksi"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion
        #region Get All Dropdown Okupasi
        public JsonResult GetAllOkupasi(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:okupasi"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion
        #region Get All Dropdown Perluasan
        public JsonResult GetAllPerluasan(string q, string page, int rowPerPage)
        {
            try
            {
                if (page == null)
                {
                    page = "1";
                }

                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    q = q,
                    page = page,
                    rowpage = rowPerPage
                };
                var token = HttpContext.Session.GetString("TOKEN");
                string Date = DateTime.Now.ToString("yyyy-MM-dd");
                string url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:DropDown:perluasan"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide hasil = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);

                return Json(hasil);
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion


        #region Selected Dropdown Perluasan
        public IActionResult SelectDataPerluasan(int id)
        {
            try
            {
                req_dataDropdown_ViewModel model = new req_dataDropdown_ViewModel()
                {
                    id = id.ToString(),
                    rowpage = 10,
                    page = "1"
                };
                var token = HttpContext.Session.GetString("TOKEN");
                var url = ConfigDataAccess.Configuration["baseAPI"] + ConfigDataAccess.Configuration["urlapi:GetDropDown:perluasan"];
                (bool resultApi, string result) = RequestToAPI.PostRequestToWebApi(url, model, token);
                ListDataDropdownServerSide data = JsonConvert.DeserializeObject<ListDataDropdownServerSide>(result);
                data = data != null ? data : new ListDataDropdownServerSide();
                return Json(data.items);
            }
            catch (Exception ex) { 
                return Json(ex);
            }

        }
        #endregion
    }
}
