﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.ViewModels
{
    public class DropdownMokupasiVM
    {
        public int ID { get; set; }
        public string NAMAOKUPASI { get; set; }
    }
    public class FilterOkupasi
    {

        public string Filter { get; set; }

    }
    public class ListDataDropdownServerSide
    {
        public int code { get; set; }
        public string message { get; set; }
        public List<DataDropdownServerSide> items { get; set; }
        public DataDropdownServerSide item { get; set; }
        public int total_count { get; set; }
    }

    public class ListDataDropdownServerSideString
    {
        public List<DataDropdownServerSideIdString> items { get; set; }
        public int total_count { get; set; }
    }

    public class DataDropdownServerSide
    {
        public int id { get; set; }
        public string idname { get; set; }
        public string text { get; set; }
        public string format_selected { get; set; }
        public string nama_text { get; set; }
        public string kode { get; set; }
        public string idparent { get; set; }
    }

    public class DataDropdownServerSideIdString
    {
        public string id { get; set; }
        public string text { get; set; }
        public string format_selected { get; set; }
        public string nama_text { get; set; }
    }
}
