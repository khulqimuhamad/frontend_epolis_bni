﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.ViewModels
{
    public class LoadPenyelesaianPremiVM
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string sortColumn { get; set; }
        public string sortColumnDir { get; set; }
        public int PageNumber { get; set; }
        public int RowsPage { get; set; }
    }
}
