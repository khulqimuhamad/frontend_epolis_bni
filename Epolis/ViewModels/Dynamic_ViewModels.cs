﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Epolis.ViewModels
{
    public class res_OutputMsg_ViewModel
    {
        public int code { get; set; }
        public string msg { get; set; }
    }

    public class req_InputById_ViewModel
    {
        public int userId { get; set; }
    }

    public class res_OutputDataMsg_ViewModel<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
        public List<T> List { get; set; }
    }

    public class res_OutputListDataMsg_ViewModel<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public List<T> Listdata { get; set; }
    }

    public class req_MenuState_ViewModels
    {
        public string unit { get; set; }
        public string path { get; set; }
        public string tokken { get; set; }
        public int? userId { get; set; }
        public int? role_Id { get; set; }
    }
    public class req_Data_ViewModels
    {
        public int Param { get; set; }
        public string Params { get; set; }
        public string tokken { get; set; }
        public int? userId { get; set; }
        public int? role_Id { get; set; }
        public string TypeSearch { get; set; }
        public string NameSearch { get; set; }
        public string SortCoulumn { get; set; }
        public string SortCoulumnDir { get; set; }
        public int PageNumber { get; set; }
        public int RowPage { get; set; }
        public int Id_PK { get; set; }
        public int Id_Penutupan { get; set; }
        public string Ids { get; set; }

    }

    public class res_Data_ViewModels<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public int? Count { get; set; }
        public List<T> List { get; set; }
    }

    public class res_RoleChanged_ViewModel<T, H>
    {
        public int code { get; set; }
        public T dataUser { get; set; }
        public List<H> Menu { get; set; }
        public string msg { get; set; }
    }

    public class req_DataById_ViewModel
    {
        public string tokken { get; set; }
        public int? userId { get; set; }
        public int? role_Id { get; set; }
        public int? DataId { get; set; }

    }
    public class res_DataById_ViewModel<T>
    {
        public int code { get; set; }
        public T Data { get; set; }
        public string msg { get; set; }
    }

    public class req_dataDropdown_ViewModel
    {
        public string id { get; set; }
        public string param { get; set; }
        public string q { get; set; }
        public string page { get; set; }
        public int rowpage { get; set; }
    }
    public class req_dataDropdownById_ViewModel
    {
        public int id { get; set; }       
    }

}
